.386
.MODEL FLAT, STDCALL
	STD_INPUT_HANDLE equ -10
	STD_OUTPUT_HANDLE equ -11
	GetStdHandle PROTO :DWORD
	atoi  PROTO :DWORD
	wsprintfA PROTO C :VARARG
	WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
	ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
	lstrlenA PROTO :DWORD
	ExitProcess PROTO :DWORD
.data
	
	cout		dd ?
	cin		    dd ?
	tekst       db "Wprowadz X znakow: ",0
	rozmiart    db $ - tekst
	wynik       db "Wynik to: %u",0
	rwynik      db $ - wynik
	bufor		dd 128 DUP(?)
	bufor2      db 10 DUP(?)
	bufor3      db 128 DUP(?)
	tablica		db 10 DUP(0)
	liczbaZ		dd ?
	liczba		dd ?	
	zmienna     db ?	
	licznik     dd 1
	suma		dd 0
	zwrot		db ?
	celo        dd ?
	alo         db ?
	sumo        db ?
	ustawiony   db 0
	adres       dd ?
	sumka       dd 0

.code
	DawajUchwyt MACRO uchwytIn :REQ, uchwytOut: REQ
		invoke GetStdHandle, uchwytIn
		mov uchwytOut, EAX
	ENDM

main proc
	DawajUchwyt STD_OUTPUT_HANDLE, cout
	DawajUchwyt STD_INPUT_HANDLE, cin

	invoke WriteConsoleA, cout, OFFSET tekst, rozmiart, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor, 9, OFFSET liczbaZ, 0
	mov eax, bufor
	mov suma, eax
	mov EAX,liczbaZ
	cmp EAX, 7d
	jg Hopsa
	sub EAX,2
	
	Hopsa:
	mov ECX, EAX
	mov licznik, ECX
;	invoke WriteConsoleA, cout, OFFSET suma, 8, OFFSET liczba, 0
	mov ESI,OFFSET bufor
	pentela:
		push ECX
		lodsb
		cmp al, 31h
		je jeden
		cmp al, 32h
		je dwa
		cmp al, 33h
		je tri
		cmp al, 34h
		je cztery
		cmp al, 35h
		je piec
		cmp al, 36h
		je szesc
		cmp al, 37h
		je siedem
		cmp al, 38h
		je osiem
		cmp al, 39h
		je dziewiec
		
		pop ECX
	cmp ECX, 1d
	dec ECX
	jne Pentela
	jmp Wyso
	jeden:
	mov EDI, OFFSET bufor3
	cmp ustawiony,1d
	je Adreso
	Powrot:
	mov al,33h
	stosb
	mov al,31h
	stosb
	mov al, 20h
	stosb
	mov ustawiony,1d
	mov adres, EDI
	jmp pentela
	dwa:
	mov EDI, OFFSET bufor3
	cmp ustawiony,1d
	je AdresoDwa
	PowrotDwa:
	mov al,33h
	stosb
	mov al,32h
	stosb
	mov al, 20h
	stosb
	mov ustawiony,1d
	mov adres, EDI
	jmp pentela
	tri:
	mov EDI, OFFSET bufor3
	cmp ustawiony,1d
	je AdresoTri
	PowrotTri:
	mov al,33h
	stosb
	mov al,33h
	stosb
	mov al, 20h
	stosb
	mov ustawiony,1d
	mov adres, EDI
	jmp pentela
	cztery:
	mov EDI, OFFSET bufor3
	cmp ustawiony,1d
	je AdresoCztery
	PowrotCztery:
	mov al,33h
	stosb
	mov al,34h
	stosb
	mov al, 20h
	stosb
	mov ustawiony,1d
	mov adres, EDI
	jmp pentela
	piec:
	mov EDI, OFFSET bufor3
	cmp ustawiony,1d
	je AdresoPiec
	PowrotPiec:
	mov al,33h
	stosb
	mov al,35h
	stosb
	mov al, 20h
	stosb
	mov ustawiony,1d
	mov adres, EDI
	jmp pentela
	szesc:
	mov EDI, OFFSET bufor3
	cmp ustawiony,1d
	je AdresoSzesc
	PowrotSzesc:
	mov al,33h
	stosb
	mov al,36h
	stosb
	mov al, 20h
	stosb
	mov ustawiony,1d
	mov adres, EDI
	jmp pentela
	siedem:
	mov EDI, OFFSET bufor3
	cmp ustawiony,1d
	je AdresoSiedem
	PowrotSiedem:
	mov al,33h
	stosb
	mov al,37h
	stosb
	mov al, 20h
	stosb
	mov ustawiony,1d
	mov adres, EDI
	jmp pentela
	osiem:
	mov EDI, OFFSET bufor3
	cmp ustawiony,1d
	je AdresoOsiem
	PowrotOsiem:
	mov al,33h
	stosb
	mov al,38h
	stosb
	mov al, 20h
	stosb
	mov ustawiony,1d
	mov adres, EDI
	jmp pentela
	dziewiec:
	mov EDI, OFFSET bufor3
	cmp ustawiony,1d
	je AdresoDziewiec
	PowrotDziewiec:
	mov al,33h
	stosb
	mov al,39h
	stosb
	mov al, 20h
	stosb
	mov ustawiony,1d
	mov adres, EDI
	jmp pentela


	;mov AL, BYTE PTR [EBX+4]
	;add  EBX,2 
	;mov AL, BYTE PTR [EBX]
	;mov EAX,  EBX
	;mov AL, BYTE PTR [EBX+2]
	;mov ECX, 4
	;Pentela:
;
;
;
	;cmp ECX, 1
	;dec ECX
	;jne Pentela
	Adreso:
	mov EDI, adres
	jmp Powrot
	AdresoDwa:
	mov EDI, adres
	jmp PowrotDwa
	AdresoTri:
	mov EDI, adres
	jmp PowrotTri
	AdresoCztery:
	mov EDI, adres
	jmp PowrotCztery
	AdresoPiec:
	mov EDI, adres
	jmp PowrotPiec
	AdresoSzesc:
	mov EDI, adres
	jmp PowrotSzesc
	AdresoSiedem:
	mov EDI, adres
	jmp PowrotSiedem
	AdresoOsiem:
	mov EDI, adres
	jmp PowrotOsiem
	AdresoDziewiec:
	mov EDI, adres
	jmp PowrotDziewiec

	Wyso:
	mov EAX, 1d
	imul EAX, licznik
	mov sumka, EAX
	mov EAX,2d
	imul EAX, licznik
	add EAX,sumka
	mov ECX, EAX
	mov ESI, OFFSET bufor3
	Wys:
		push ECX
		lodsb 
		mov alo, al
		
		invoke WriteConsoleA, cout, OFFSET alo, 1, OFFSET liczba, 0
		
		pop ECX
	cmp ECX, 1d
	dec ECX
	jne Wys
	
	Koniec:
	;invoke wsprintfA, OFFSET bufor2, OFFSET wynik, alo
;	invoke WriteConsoleA, cout, OFFSET alo, 8, OFFSET liczba, 0
invoke ExitProcess, 0
main endp
ScanBin  PROC 
;; funkcja ScanInt przekszta�ca ci�g cyfr dziesi�tnych do liczby, kt�r� jest zwracana przez EAX 
;; argument - zako�czony zerem wiersz z cyframi 
;--- pocz�tek funkcji 
   push   EBP 
   mov   EBP, ESP   ; wska�nik stosu ESP przypisujemy do EBP 
;--- odk�adanie na stos 
   push   EBX 
   push   ECX 
   push   EDX 
   push   ESI 
   push   EDI 
;--- przygotowywanie cyklu 
   mov   EBX, [EBP+8] 
   push   EBX 
   call   lstrlenA 
   mov   EDI, EAX   ;liczba znak�w 
   mov   ECX, EAX   ;liczba powt�rze� = liczba znak�w 
   xor   ESI, ESI   ; wyzerowanie ESI 
   xor   EDX, EDX   ; wyzerowanie EDX 
   xor   EAX, EAX   ; wyzerowanie EAX 
   mov   EBX, [EBP+8] ; adres tekstu
;--- cykl -------------------------- 
pocz: 
   cmp   BYTE PTR [EBX+ESI], 0h   ;por�wnanie z kodem \0 
   jne   @F 
   jmp   et4 
@@: 
   cmp   BYTE PTR [EBX+ESI], 0Dh   ;por�wnanie z kodem CR 
   jne   @F 
   jmp   et4 
@@: 
   cmp   BYTE PTR [EBX+ESI], 0Ah   ;por�wnanie z kodem LF 
   jne   @F 
   jmp   et4 
@@: 
   cmp   BYTE PTR [EBX+ESI], 02Dh   ;por�wnanie z kodem - 
   jne   @F 
   mov   EDX, 1 
   jmp   nast 
@@: 
   cmp   BYTE PTR [EBX+ESI], 030h   ;por�wnanie z kodem 0 
   jae   @F 
   jmp   nast 
@@: 
   cmp   BYTE PTR [EBX+ESI], 031h   ;por�wnanie z kodem 9 
   jbe   @F 
   jmp   nast 
;---- 
@@:    
   push   EDX   ; do EDX procesor mo�e zapisa� wynik mno�enia 
   mov   EDI, 2 
   mul   EDI      ;mno�enie EAX * EDI 
   mov   EDI, EAX   ; tymczasowo z EAX do EDI 
   xor   EAX, EAX   ;zerowani EAX 
   mov   AL, BYTE PTR [EBX+ESI] 
   sub   AL, 030h   ; korekta: cyfra = kod znaku - kod 0    
   add   EAX, EDI   ; dodanie cyfry 
   pop   EDX 
nast:   
   inc   ESI 
   loop   pocz 
;--- wynik 
   or   EDX, EDX   ;analiza znacznika EDX 
   jz   @F 
   neg   EAX 
@@:    
et4:;--- zdejmowanie ze stosu 
   pop   EDI 
   pop   ESI 
   pop   EDX 
   pop   ECX 
   pop   EBX 
;--- powr�t 
   mov   ESP, EBP   ; przywracamy wska�nik stosu ESP
   pop   EBP 
   ret	4
ScanBin   ENDP 
END
